package services.impl;

import org.elasticsearch.search.aggregations.bucket.histogram.ExtendedBounds;
import org.joda.time.DateTime;

import org.apache.http.HttpHost;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.*;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import play.libs.Json;

import services.DataService;
import services.dtos.*;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Singleton
public class DataServiceImpl implements DataService {
    private final String INDEX_NAME = "quality-check";
    private final String DOC_TYPE = "itemQuality";
    private AtomicInteger eventCounter=new AtomicInteger(0);

    private final RestClientBuilder clientBuilder = RestClient.builder(new HttpHost("localhost", 9200));

    private final RestHighLevelClient highLevelClient = new RestHighLevelClient(
            clientBuilder.setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder)
    );

    @Inject
    public DataServiceImpl(){

    }


    @Override
    public IndexResponse addNewEvent(ItemQuality itemQuality) throws IOException {
        IndexRequest request = new IndexRequest(INDEX_NAME).type(DOC_TYPE).id(Integer.toString(eventCounter.getAndIncrement()))
                .source(Json.stringify(Json.toJson(itemQuality)), XContentType.JSON);
        return highLevelClient.index(request, RequestOptions.DEFAULT);
    }


    @Override
    public List<QualityOverTimeAggregator> getLastTenRecords() throws IOException {

        SearchSourceBuilder query = new SearchSourceBuilder()
                .query(QueryBuilders.boolQuery()
                        .filter(QueryBuilders.rangeQuery("time").from("now/d-1d").timeZone("+05:30")))
                .aggregation(
                        AggregationBuilders.dateHistogram("QUALITY_OVER_TIME").field("time").dateHistogramInterval(DateHistogramInterval.minutes(1))
                            .subAggregation(AggregationBuilders.terms("QUALITY_AGGREGATION").field("quality"))
                );

        SearchRequest request = new SearchRequest(INDEX_NAME).types(DOC_TYPE).source(query.from(0).size(0));
        SearchResponse response = highLevelClient.search(request, RequestOptions.DEFAULT);
        return aggregationParser(response);
    }

    public ChartData getChartDefects() throws IOException {
        long totalData;
        long totalDefects=0;
        double defectsRatio;

        SearchSourceBuilder daily_query = new SearchSourceBuilder()
                .query(QueryBuilders.boolQuery()
                        .filter(QueryBuilders.rangeQuery("time").from("now/d").timeZone("+05:30")))
                .aggregation(
                        AggregationBuilders.terms("QUALITY_AGGREGATION").field("quality")
                );

        SearchRequest request = new SearchRequest(INDEX_NAME).types(DOC_TYPE).source(daily_query.from(0).size(0));

        SearchResponse response = highLevelClient.search(request, RequestOptions.DEFAULT);

        totalData=response.getHits().totalHits;

        MultiBucketsAggregation aggregations = response.getAggregations().get("QUALITY_AGGREGATION");
        for(MultiBucketsAggregation.Bucket bucket:aggregations.getBuckets()){
            if(bucket.getKey().equals("fail"))
                totalDefects=bucket.getDocCount();
        }

        defectsRatio=totalDefects*1.0/totalData;
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        defectsRatio =Double.parseDouble(decimalFormat.format(defectsRatio));

        SearchSourceBuilder lastSetOfPeriodQuery = new SearchSourceBuilder()
                .query(QueryBuilders.boolQuery()
                        .filter(QueryBuilders.rangeQuery("time").from("now/m-10m").to("now").timeZone("+05:30"))
                        .must(QueryBuilders.matchQuery("quality","fail")))
                .aggregation(
                        AggregationBuilders.dateHistogram("DEFECTS_OVER_TIME").field("time")
                                .dateHistogramInterval(DateHistogramInterval.minutes(1))
                                .minDocCount(0)
                                .extendedBounds(new ExtendedBounds("now/m-10m","now"))
                );

        SearchRequest periodRequest = new SearchRequest(INDEX_NAME).types(DOC_TYPE).source(lastSetOfPeriodQuery.from(0).size(0));
        SearchResponse periodResponse = highLevelClient.search(periodRequest, RequestOptions.DEFAULT);

        MultiBucketsAggregation defectsAggregations = periodResponse.getAggregations().get("DEFECTS_OVER_TIME");

        List<IntervalDefects> defects= new ArrayList<>();
        for(MultiBucketsAggregation.Bucket bucket: defectsAggregations.getBuckets()){
            Date time = ((DateTime) bucket.getKey()).toDate();
            Long defectsCount = bucket.getDocCount();
            defects.add(new IntervalDefects(time,defectsCount));
        }

        return new ChartData(totalData,totalDefects,defectsRatio,defects);
    }


    private List<QualityOverTimeAggregator> aggregationParser(SearchResponse searchResponse) {
        List<QualityOverTimeAggregator> results = new ArrayList<>();

        MultiBucketsAggregation aggregations = searchResponse.getAggregations().get("QUALITY_OVER_TIME");

        for (MultiBucketsAggregation.Bucket externalBucket : aggregations.getBuckets()) {
            String interval = externalBucket.getKey().toString();
            long intervalCount = externalBucket.getDocCount();
            MultiBucketsAggregation internalBuckets = externalBucket.getAggregations().get("QUALITY_AGGREGATION");

            List<QualityAggregator> values = new ArrayList<>();

            for (MultiBucketsAggregation.Bucket internalBucket : internalBuckets.getBuckets()) {
                String quality = internalBucket.getKey().toString();
                long count = internalBucket.getDocCount();
                values.add(new QualityAggregator(quality, count));
            }
            results.add(new QualityOverTimeAggregator(interval, intervalCount, values));
        }

        return results;
    }

}
