package services.dtos;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;

public class ItemQualityErrorLog {
    public String item="";
    public String time="";
    public String quality="";
    public String ERROR="";

    public ItemQualityErrorLog(String record, String ERROR) {
        JsonNode error = Json.parse(record);
        try {
            this.item = error.get("item").asText();
        }
        catch(NullPointerException nullPointer) {
            this.item ="";
        }

        try {
            this.time = error.get("time").asText();
        }
        catch(NullPointerException nullPointer) {
            this.time ="";
        }

        try {
            this.quality = error.get("quality").asText();
        }
        catch(NullPointerException nullPointer) {
            this.quality ="";
        }

        this.ERROR = ERROR;
    }
}
