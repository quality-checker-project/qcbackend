package services.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ItemQuality {
    public String item;
    @JsonDeserialize(using = DateHandler.class)
    public Date time;
    public String quality;

    @Override
    public String toString() {
        return "{" +
                "\"item\":\"" + item + "\"" +
                ", \"time\":\"" + time + "\"" +
                ", \"quality\":\"" + quality + "\"" +
                "}";
    }

    public ItemQuality(String item, Date time, String quality) {
        this.item = item;
        this.time = time;
        this.quality = quality;
    }

    public ItemQuality(){
    }

}
