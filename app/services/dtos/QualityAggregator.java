package services.dtos;

public class QualityAggregator {
    String quality;
    long count;

    public QualityAggregator(String quality, long count) {
        this.quality = quality;
        this.count = count;
    }

    public QualityAggregator() {
    }

    @Override
    public String toString() {
        return "QualityAggregator{" +
                "key='" + quality + '\'' +
                ", count=" + count +
                '}';
    }
}
