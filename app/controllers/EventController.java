package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import play.mvc.*;
import services.DataService;
import services.KafkaEventProcessorService;
import services.dtos.ItemQuality;
import services.dtos.ItemQualityCollection;

import javax.inject.*;
import java.util.Arrays;
import java.util.List;

@Singleton
public class EventController extends Controller {
    private KafkaEventProcessorService kafkaEventProcessorService;

    @Inject
    public EventController(KafkaEventProcessorService kafkaEventProcessorService){
        this.kafkaEventProcessorService=kafkaEventProcessorService;
    }

    public Result addEvent(Http.Request request){
        kafkaEventProcessorService.addToTopic("quality", request.body().asJson().toString());
        return Results.ok("{\"status\":\"success\"}");
    }

    public Result addEventCollection(Http.Request request) throws JsonProcessingException {
        String jsonCollection = request.body().asJson().toString();
        ObjectMapper mapper = new ObjectMapper();
        List<ItemQualityCollection> itemCollection= Arrays.asList(mapper.readValue(jsonCollection, ItemQualityCollection[].class));
        for(ItemQualityCollection item :itemCollection){

            if(item.passCount>0){
                ItemQuality itemQuality = new ItemQuality(item.item,item.time,"pass");
                for (int i = 0; i < item.passCount ; i++) {
                    kafkaEventProcessorService.addToTopic("quality",itemQuality.toString());
                }
            }

            if(item.failCount>0){
                ItemQuality itemQuality = new ItemQuality(item.item,item.time,"fail");
                for (int i = 0; i < item.passCount ; i++) {
                    kafkaEventProcessorService.addToTopic("quality",itemQuality.toString());
                }
            }
        }
        return Results.ok();
    }


}
