package controllers;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import services.DataService;
import services.dtos.ChartData;
import services.dtos.QualityOverTimeAggregator;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.util.List;

@Singleton
public class DataController extends Controller {
    private DataService dataService;

    @Inject
    public DataController(DataService dataService){
        this.dataService=dataService;
    }

    public Result getLastTenRecords() throws IOException {
        List<QualityOverTimeAggregator> quality=dataService.getLastTenRecords();
        dataService.getChartDefects();
        return Results.ok(Json.toJson(quality));
    }

    public Result getChartData() throws IOException {
        ChartData defectsData =dataService.getChartDefects();
        return Results.ok(Json.toJson(defectsData));
    }

}
